function [mSD, varSD, BOLD, stdBOLD, time_maxZ] = fMRI_smoother( ROI_BOLD, std_ROI_BOLD, tobs, subdir_id )
% Estimate the smoothing distribution of a single node of a DCM where the
% hidden variables (z,s,f,v,q) are all noisy. The Parameters of the model
% are fixed below. The particles are initialized as wide, but can be
% changed with the flags below.
plots = 0;

%% Flags
%Choose type of inizialisation
pri='wide';
%Cases
noisy=1; % 1 = true and all variables are noisy; 0 = false, then only neuronal noise considered.
sys='DCM';
diff_case = sys;

%% Define Problem
disp('Definig Problem...')

Yx = ROI_BOLD;
sigY = std_ROI_BOLD;
fprintf('Observation noise is now: %1.4f \n', sigY)
Time = tobs(end);
Dtobs = diff(tobs(end-1:end));

h = Dtobs/40;
obs = int64((tobs/h)+1);
T = int64(Time/h);
taxis=(0:(double(T)-1))*h;

%%%%%%% MODEL %%%%%%%%%%%%%
% Neuronal Model
A=-1;
B=0;
C=0;
t_on = 0;
t_off = 0;
% Hemodynamic system: parameters for s 
epsilon=0.8;
tau_s=1.54;
tau_f=2.44;

%Balloon model: v and q
tau_0=1.02;
alpha=0.32;
alpha=1/alpha;
E_0=0.4;

%BOLD Signal
V_0=0.018;
k1=7*E_0;
k2=2;
k3=2*E_0-0.2;

%% Initializations
% Initialize state X = [z;s;f;v;q]
%Initial Condition
switch pri
    case 'wide'
        sigma_z0 = 0.11;
        sigma_s0 = 0.075;
        sigma_f0 = 0.15;
        sigma_v0 = 0.05;
        sigma_q0 = 0.06;
    case 'fixed'
        pos='fixed'
        sigma_z0 = 0;
        sigma_s0 = 0;
        sigma_f0 = 0;
        sigma_v0 = 0;
        sigma_q0 = 0;
end

 mean_prior=[0,0,1,1,1];
 std_prior=[ sigma_z0, sigma_s0, sigma_f0, sigma_v0, sigma_q0];

%% Noise
sigZ = 0.15;
switch noisy
    case 1
        sigS=0.05;
        sigF=0.15;
        sigV=0.01;
        sigQ=0.01;
    case 0
        sigS=0;
        sigF=0;
        sigV=0;
        sigQ=0;
end

param_Z = [t_on,t_off,A,B,C];
param_HD = [epsilon,tau_s,tau_f];
param_Balloon = [tau_0,alpha,E_0];
param = [param_Z,param_HD,param_Balloon];
% param = [ t_on ,t_off, A, B, C, epsilon, tau_s, tau_f, tau_0, alpha, E_0 ];
param(5)=0;
disp('Input switched off!')

%load('Data_statedepNoise_sigY_0.001.mat', 'sigZ', 'sigS','sigF','sigV','sigQ')
%load('ParametersAPIS_InpOFF_VIS_ROI.mat', 'param_signal', 'param')

%Param of DCM
D=5;
noise_factor = 1; % with higher values there is not much difference (the estimations with higher noise factor have more highs and vallies)
if noise_factor>1
fprintf('Neuronal noise increased by %1.2f \n! ',noise_factor)
end
dyn_var = [noise_factor*sigZ^2,sigS^2,sigF^2,sigV^2,sigQ^2];
fprintf('Neuronal noise is now: %1.3f \n', dyn_var(1))
% Param BOLD
signal_type = 'BOLD';
param_signal = [V_0,k1,k2,k3];

% Param Observ. model
obs_model = 'Gauss';
yvar=sigY^2;
param_obs = yvar;

%% SMOOTHER        
%Parameters of APIS
pos='adapt';
N=50000;
iters=800;
ESSstop=0.1;
learning_rate=0.005;
gamma=100/N;
beta=1.15;
extra = 'no'; %'cost' enables calculation of mean cost and its variance
param_disp = fprintf('N = %i, eta = %1.3f, iters = %i, ESSstop = %1.2f \n',N,learning_rate,iters,ESSstop);

APIS_annealing

BOLD =wend'*squeeze(Obs_Signal);
stdBOLD = sqrt(var(squeeze(Obs_Signal),wend));
time_maxZ = taxis(mSD(:,1)==max(mSD(:,1)));

if ~FAILED
cd(subdir_id)
save('ParamsAPIS_BOLD','D','param','dyn_var','noise_factor','signal_type','param_signal','obs_model','yvar','param_obs','pri','pos','N','iters','ESSstop','learning_rate','gamma','beta','extra')
save('Data_plots_BOLD','PSESS','ESS_raw','taxis','mSD','varSD','BOLD','stdBOLD','obs','h','Yx','b', 'A','time_maxZ')
save('Results_BOLD')
disp('Data Saved in ')
disp(pwd)
cd('../../../')

end

switch plots
    case 0
        disp('Plots disabled')
    case 1
        %SMOOTHING ESTIMATE
        figure;
        for dd=1:D
            subplot(2,3,dd)
            hold on
            plot(taxis,mSD(:,dd),'r')
            plot(taxis,mSD(:,dd)+sqrt(varSD(:,dd)),':r')
            plot(taxis,mSD(:,dd)-sqrt(varSD(:,dd)),':r')
            if dd==1
                plot(taxis,I,'k')
            end
            hold off
        end
        subplot(2,3,6)
        
        hold on
        BOLD =wend'*squeeze(Obs_Signal);
        stdBOLD = sqrt(var(squeeze(Obs_Signal),wend));
        plot(taxis,BOLD,'r')
        plot(taxis,BOLD+stdBOLD,':r')
        plot(taxis,BOLD-stdBOLD,':r')
        plot(obs*h,Yx,'dr','MarkerFace','r')
        hold off
        
        figure
        for dd=1:D
            subplot(2,3,dd)
            plot(squeeze(var(PS(:,dd,:))),'b')
            hold on
            plot(varSD(:,dd),'r')
            hold off
        end
        subplot(2,3,6)
        plot(var(squeeze(Obs_Signal),wend),'b')
        hold on
        plot(stdBOLD.^2,'r')
        hold off
        
        % OPEN LOOP CONTROLLER
        figure
        subplot(1,2,1)
        plot(taxis,b)
        legend('b_1','b_2','b_3','b_4','b_5')
        STR=sprintf('Open loop controller');
        title(STR)
        % Diagonal elements of the feedback matrix
        subplot(1,2,2)
        hold on
        plot(taxis,squeeze(A(1,1,:)),'b')
        plot(taxis,squeeze(A(2,2,:)),'g')
        plot(taxis,squeeze(A(3,3,:)),'k')
        plot(taxis,squeeze(A(4,4,:)),'c')
        plot(taxis,squeeze(A(5,5,:)),'m')
        legend('A_{1,1}','A_{2,2}','A_{3,3}','A_{4,4}','A_{5,5}')
        plot(h*(obs-1),0,'.r')
        STR=sprintf('Feedback controller; N=%i',N);
        title(STR)
        hold off
        % The feedback matrix
        figure
        for dd=1:D
            for gg=1:D
                subplot(5,5,(dd-1)*D+gg)
                plot(taxis,squeeze(A(dd,gg,:)))
                axis([0 taxis(end) -inf inf])
                hold on
                plot(h*(obs-1),0,'dr','MarkerFace','r')
                hold off
            end
        end
        % ESS
        STR=sprintf('ESS over iterations, Initialization: %s, N=%i',pri,N);
        figure;
        plot(PSESS,'-b')
        hold on;
        plot(ESS_raw,'r')
        hold off
        label('ESS','raw ESS')
        title(STR)
        
        %EXTRAS
        switch extra
            case 'cost'
                STR=sprintf('E_{u}[S_{0}] and <S_{0}> vs iters, Initialization: %s, N=%i, smoother=%1.2f',pri,N,a);
                figure;
                plot(mS0_iter,'b')
                hold on
                plot(wmS0_iter,'k')
                legend('E_{u}[S_{0,u}]','<S_{0,u}>')
                plot(1:iters,mS0_iter+stdS0_iter,'-.r',1:iters,mS0_iter-stdS0_iter,'-.r')
                plot(1:iters,wmS0_iter+wstdS0_iter,':k',1:iters,wmS0_iter-wstdS0_iter,':k')
                title(STR)
                hold off
        end
        
end

end
                                                                                                                                                                                                                       