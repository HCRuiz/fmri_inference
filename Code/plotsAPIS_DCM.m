% load('Y:\hruiz\fMRI_inference\Code\Data_plots_statedepNoise.mat')
% load('Y:\hruiz\fMRI_inference\Code\Parameters_APIS_InpOFF_trafo_f_statedepNoise_data.mat')
set(0,'defaulttextfontsize',18);
set(0,'defaultaxesfontsize',18);
set(0,'defaultfigurecolor',[1 1 1]);
pri='wide'


%% ESS
  STR=sprintf('ESS over iterations, Initialization: %s, N=%i, eta=%.3f',pri,N,learning_rate);
        figure;
        semilogy(PSESS,'-b')
        hold on;
        semilogy(ESS_raw,'-r')
        hold off
        title(STR)
%% SD Estimate        
        figure;
        for dd=1:D
            subplot(2,3,dd)
        hold on
%         plot(taxis,X(dd,:),'b')
        plot(taxis,mSD(:,dd),'r')
        plot(taxis,mSD(:,dd)+sqrt(varSD(:,dd)),':r')
        plot(taxis,mSD(:,dd)-sqrt(varSD(:,dd)),':r')
        hold off
        end
        subplot(2,3,6)
        hold on
%         plot(taxis,y,':b')
        plot(taxis,BOLD,'r','LineWidth',2)
        plot(taxis,BOLD+stdBOLD,':r','LineWidth',2)
        plot(taxis,BOLD-stdBOLD,':r','LineWidth',2)
        plot(double(obs)*h,Yx,'dr','MarkerFace','g')
        hold off
 %% Controller       
        figure
        subplot(1,2,1)
        plot(taxis,b)
        legend('b_1','b_2','b_3','b_4','b_5')
        hold on
        plot(h*(obs-1),0,'dr','MarkerFace','r')
        hold off
        STR=sprintf('Open loop controller');
        title(STR)
        
        subplot(1,2,2)
        hold on
        % for dd=1:D
        %     plot(taxis,squeeze(A(dd,dd,:)))
        % end
        plot(taxis,squeeze(A(1,1,:)),'b')
        plot(taxis,squeeze(A(2,2,:)),'g')
        plot(taxis,squeeze(A(3,3,:)),'r')
        plot(taxis,squeeze(A(4,4,:)),'c')
        plot(taxis,squeeze(A(5,5,:)),'m')
        legend('A_{1,1}','A_{2,2}','A_{3,3}','A_{4,4}','A_{5,5}')
        plot(h*(obs-1),0,'*k','MarkerFace','k')
        STR=sprintf('Feedback controller; N=%i',N);
        title(STR)
        hold off
        
        figure
        for dd=1:D
            for gg=1:D
            subplot(5,5,(dd-1)*D+gg)
            plot(taxis,squeeze(A(dd,gg,:)))
            axis([0 taxis(end) -inf inf])
            hold on
            plot(h*(obs-1),0,'dr','MarkerFace','r')
            hold off
            end
        end
        
%% Comparisons of PS and SD
% figure
% for dd=1:D
% subplot(2,3,dd)
% plot(squeeze(var(PS(:,dd,:))),'b')
% hold on
% plot(varSD(:,dd),'r')
% hold off
% end
% figure
% step=ceil(diff(obs(1:2))/2);
% for tt=1:length(obs)
%     subplot(2,length(obs),tt)
%      plot(PS(:,obs(tt)-step),wend,'.')
%     xlabel(num2str((obs(tt)-step)*h))
%     hold on
%     plot(mSD(obs(tt)-step),0,'r+')
%     plot(mean(PS(:,obs(tt)-step)),0,'*g')
%     hold off
% end
% for tt=1:length(obs)
%     subplot(2,length(obs),length(obs)+tt)
%     plot(PS(:,obs(tt)),wend,'.')
%     xlabel(num2str(obs(tt)*h))
%     hold on
%     plot(mSD(obs(tt)),0,'r+')
%     plot(mean(PS(:,obs(tt))),0,'*g')
%     hold off
% end

%% PLOTS
% figure
% subplot(1,2,1)
% plot(taxis,b)
% legend('b_1','b_2','b_3')
% hold on
%  plot(taxis,bexact,'k','LineWidth',1)
% hold off
% STR=sprintf('Open loop controller');
% title(STR)
% 
% subplot(1,2,2)
% plot(taxis,squeeze(A1(1,1,:)),'b')
% hold on
% plot(taxis,squeeze(A1(2,2,:)),'g')
% plot(taxis,squeeze(A1(3,3,:)),'k')
% legend('A_{1,1}','A_{2,2}','A_{3,3}')
% plot(h*(obs-1),0,'.r')
% plot(taxis,Aexact,'r','LineWidth',1)
% STR=sprintf('Feedback controller; N=%i, smoother=%1.2f',N,a);
% title(STR)
% hold off
% 
% STR=sprintf('ESS over iterations, Initialization: %s, N=%i, smoother=%1.2f',pri,N,a);
% figure;
% plot(PSESS,'-b')
% title(STR)
% % 
% % STR=sprintf('E_{u}[S_{0}] and <S_{0}> vs iters, Initialization: %s, N=%i, smoother=%1.2f',pri,N,a);
% % figure;
% % plot(mS0_iter,'b')
% % hold on
% % plot(wmS0_iter,'k')
% % legend('E_{u}[S_{0,u}]','<S_{0,u}>')
% % plot(1:iters,mS0_iter+stdS0_iter,'-.r',1:iters,mS0_iter-stdS0_iter,'-.r')
% % plot(1:iters,wmS0_iter+wstdS0_iter,':k',1:iters,wmS0_iter-wstdS0_iter,':k')
% % title(STR)
% % hold off

% % figure;
% % plot(1:loops,mean_PS0)
% % title('Mean of the posterior at t=0 used to build up the KDE')