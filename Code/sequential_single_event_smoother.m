%% Sequential single event smoother
% Loads the data given by single event BOLD time series and applies APIS to
% each of them sequentially

clear all
addpath  Z://hruiz//APIS_annealing//General_functions%/vol/snn4/hruiz/APIS_annealing/General_functions %
addpath Y://hruiz//fMRI_inference//Data//SUBJECT1%/vol/snn3/hruiz/fMRI_inference/Data/SUBJECT1 %
addpath Y://hruiz//fMRI_inference//Code%/vol/snn3/hruiz/fMRI_inference/Code

%% Change file id  'BOLD_TS_*' where * can be M,V,A for Motor, Auditory or Visual
disp('LOADING DATA...')
file_id = 'BOLD_TS_V';
data_file = strcat(file_id,'.mat');
load(data_file)
file_id = strcat('SUBJECT1/',file_id)
mkdir(file_id)

%% CHANGE HERE also std_*_BOLD
% [number_of_TS , trials] = size(std_V_BOLD,2);
number_of_TS = size(std_V_BOLD,1);
fprintf('Total number of jobs: %i \n', number_of_TS)

time_maxEZ_SD = zeros(1,number_of_TS);
Expected_time_maxZ_SD = zeros(1,number_of_TS);
time_to_estimate = zeros(1,number_of_TS);

trial =2
for ts = 1:number_of_TS
    tic
    subdir = sprintf('trial%i/ts%i',trial,ts)
    subdir_id = strcat(file_id,'/',subdir);
    mkdir(subdir_id)
    ROI_BOLD = V_BOLD(ts,:,trial);
    std_ROI_BOLD = std_V_BOLD(ts,trial);
    [mSD, varSD, BOLD, stdBOLD, time_maxEZ,Expected_time_maxZ] = fMRI_smoother( ROI_BOLD, std_ROI_BOLD, tobs, subdir_id );
    time_maxEZ_SD(ts) =  time_maxEZ;
    Expected_time_maxZ_SD(ts) =  Expected_time_maxZ;
    time_to_estimate(ts)  = toc
end
dir_id = sprintf('trial%i',trial)
dir_id = strcat(file_id,'/',dir_id);
cd(dir_id)
save('time_peak_Z','time_maxEZ_SD','Expected_time_maxZ_SD')

cd('../../../')
disp(pwd)
disp('DONE!')

% plotsAPIS_DCM