clear all

%addpath Y://hruiz//fMRI_inference//Data//SUBJECT1//%
addpath Y://hruiz//fMRI_inference//Code
addpath D://fMRIdata_Wietske//SUBJECT1

trial= 2%size(std_M_BOLD,2);
number_of_TS = 23%size(std_M_BOLD,1);

response_times = sprintf('RESPT%i.txt',trial)
fileID = fopen(response_times);
RESPT = fscanf(fileID,'%i',[number_of_TS,1]); %are these in ms?
% event_times = sprintf('starts%i_TR04.txt',ii)
% fileID = fopen(event_times);
% starts_TR04(:,:,ii) = fscanf(fileID,'%f',[3,10])'; %this in seconds?
fclose('all')

cd('Y://hruiz//fMRI_inference//Code')
disp('LOADING DATA...')
file_id = 'BOLD_TS_M';
file_id = strcat('SUBJECT1/',file_id)

time_maxZ_SD1 = zeros(number_of_TS,1);
time_maxBOLD_SD1 = zeros(number_of_TS,1);
time_maxOBS = zeros(number_of_TS,1);
Z =  zeros(1640,number_of_TS);
B = zeros(1640,number_of_TS);
OBS = zeros(42,number_of_TS);
OLC = zeros(1640,5,25);%number_of_TS);


for ts = 1:number_of_TS
    
    subdir = sprintf('trial%i_ts%i',trial,ts)
    subdir_id = strcat(file_id,'/',subdir);
    cd(subdir_id)
    load('ParamsAPIS_BOLD.mat')
    load('Data_plots_BOLD.mat')
    time_maxZ_SD1(ts) =  time_maxZ;
    time_maxBOLD_SD1(ts) =  taxis(find(BOLD==max(BOLD)));
    time_maxOBS(ts) = (double(obs(find(Yx==max(Yx))))-1)*h;
    Z(:,ts) = mSD(:,1);
    B(:,ts) = BOLD';
    OBS(:,ts) = Yx';
    OLC(:,:,ts) = b';
    cd('../../../')
    pwd
end

figure;
plot(6.2+(RESPT(1:number_of_TS)+150)/1000,time_maxZ_SD1(1:number_of_TS),'*')
title('Response time (RESPT) vs time of max. neuronal activity')
xlabel('6.2+(RESPT+150)/1000')
ylabel('t(Z_{max})')
 %size(time_maxZ_SD1,1)
 diff_estm_and_measured_RESPT = time_maxZ_SD1(1:number_of_TS)-(6+(RESPT(1:number_of_TS)+150)/1000);
%  mean_diff = mean(diff_estm_and_measured_RESPT)
 figure
 plot(diff_estm_and_measured_RESPT,'-o')
%  hist(diff_estm_and_measured_RESPT,50)
 
figure;
subplot(2,1,1)
plot(time_maxZ_SD1,time_maxBOLD_SD1,'-*')
xlabel('t(Z_{max})')
ylabel('t(BOLD_{max})')
title('Time of maximal estimated neuronal activity vs time of maximal estimated BOLD')
subplot(2,1,2)
%plot(max(B),max(OBS),'-*r')
plot(time_maxBOLD_SD1,max(B),'-*r')
xlabel('t(BOLD_{max})')
ylabel('BOLD_{max}')

figure;
subplot(2,1,1)
plot(taxis,Z)
axis([0 taxis(end) -inf inf])
xlabel('t in sec.')
ylabel('Neuronal Activity')
subplot(2,1,2)
plot(taxis,B,time_maxBOLD_SD1,max(B),'*')
xlabel('t in sec.')
ylabel('Bold')
axis([0 taxis(end) -inf inf])

figure
plot(time_maxBOLD_SD1,'-*g')
xlabel('Event numer')
ylabel('t(BOLD_{max})')

figure
plot(max(B),'-o')
xlabel('Event numer')
ylabel('BOLD_{max}')

figure
plot(max(OBS),'-o')
xlabel('Event numer')
ylabel('OBS_{max}')

figure
plot(time_maxOBS,'-*')
xlabel('Event numer')
ylabel('t(OBS_{max})')

figure
D1 = 5;
D2 = 5;
for dd=1:D1
    for gg=1:D2
        subplot(D1,D2,(dd-1)*D1+gg)
        plot(taxis,OLC(:,:,(dd-1)*D1+gg))
        xlabel('t in sec.')
        ylabel('Open loop')
        axis([0 taxis(end) -inf inf])
        hold on
        plot(h*(obs-1),0,'dr','MarkerFace','r')
        hold off
    end
end

figure
D1 = 5;
D2 = 5;
for dd=1:D1
    for gg=1:D2
        if (dd-1)*D1+gg > size(Z,2)
            break
        end
        subplot(D1,D2,(dd-1)*D1+gg)
        plot(taxis,Z(:,(dd-1)*D1+gg))
        axis([0 taxis(end) -inf inf])
        xlabel('t in sec.')
        ylabel('Neuronal Activ.')
        hold on
        plot(h*(obs-1),0,'dr','MarkerFace','r')
        hold off
    end
end

disp('LOADING DATA...')
file_id = 'BOLD_TS_M';
data_file = strcat(file_id,'.mat');
load(data_file)

figure
D1 = 5;
D2 = 5;
for dd=1:D1
    for gg=1:D2
        if (dd-1)*D1+gg > size(M_BOLD,1)
            break
        end
        subplot(D1,D2,(dd-1)*D1+gg)
        plot(tobs,M_BOLD((dd-1)*D1+gg,:,trial))
        axis([0 tobs(end) -inf inf])
        xlabel('t in sec.')
        ylabel('Observed Bold')
    end
end