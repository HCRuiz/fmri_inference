% clear all
% addpath Z:/hruiz/APIS_annealing/General_functions
% sys='DCM'
% diff_case = sys;
% plots = 1;

% set(0,'defaulttextfontsize',18);
% set(0,'defaultaxesfontsize',18);
% set(0,'defaultfigurecolor',[1 1 1]);
%% Flags
%Choose type of inizialisation
pri='wide'
%Cases
noisy=1 % 1 = true and all variables are noisy; 0 = false, then only neuronal noise considered. 
%% GENERATE DATA

%PARAMETERS
%Integration period
Time=12; %Time horizon in seconds
T=Time*100; % Number of Integration steps
h=Time/T; %integration step
taxis=h*(0:T-1);
%Input: Box car function
t_on=400;
t_off=415;
I=zeros(1,T);
I(t_on:t_off) = 1;
% I(t_on:end) = 5*exp(-dt*(1:(int_steps-(t_on-1))));

% Neuronal Model
A=-1;
B=0;
C=5;

% Hemodynamic system: parameters for s 
epsilon=0.8;
tau_s=1.54;
tau_f=2.44;

%Balloon model: v and q
tau_0=1.02;
alpha=0.32;
alpha=1/alpha;
E_0=0.4;

%BOLD Signal
y=zeros(1,T);
V_0=0.018;
k1=7*E_0;
k2=2;
k3=2*E_0-0.2;

%% Initializations
% Initialize state X = [z;s;f;v;q]
X = zeros(5,T); 
%Initial Condition
switch pri
    case 'wide'
        sigma_z0 = 0.11;
        sigma_s0 = 0.075;
        sigma_f0 = 0.15;
        sigma_v0 = 0.05;
        sigma_q0 = 0.06;
    case 'fixed'
        pos='fixed'
        sigma_z0 = 0;
        sigma_s0 = 0;
        sigma_f0 = 0;
        sigma_v0 = 0;
        sigma_q0 = 0;
end

 mean_prior=[0,0,1,1,1];
 std_prior=[ sigma_z0, sigma_s0, sigma_f0, sigma_v0, sigma_q0];

X(:,1) = mean_prior' + std_prior'.*randn(5,1);

sigY = 0.001; %is this choice correct? with 0.007 is the neuronal activity infered very close to the prior process... not very impresive
y(:,1) = V_0*(k1*(1-X(5,1)) + k2*(1-X(5,1)./X(4,1)) + k3*(1-X(4,1))) + sigY*randn;

%Noise
dW=sqrt(h)*randn(5,T);
sigZ = 0.15;
switch noisy
    case 1
        sigS=0.05;
        sigF=0.15;
        sigV=0.01;
        sigQ=0.01;
    case 0
        sigS=0;
        sigF=0;
        sigV=0;
        sigQ=0;
end

param_Z = [t_on,t_off,A,B,C];
param_HD = [epsilon,tau_s,tau_f];
param_Balloon = [tau_0,alpha,E_0];
param = [param_Z,param_HD,param_Balloon];
SIGMA = [sigZ;sigS;sigF;sigV;sigQ];

%% Integration
%% for t=1:T-1
%     %Neural activity
%     z(:,t+1) = z(:,t) + (A+I(t)*B)*z(:,t)*h + C*I(t)*h + sigZ*dWz(:,t) ;
%     %Hemodynamic equations
%     s(:,t+1) = s(:,t) + (epsilon*z(:,t) - 1/tau_s*s(:,t) - 1/tau_f*(f(:,t)-1))*h + sigS*dWs(:,t) ;
%     switch diff_case
%         case 'DCM'
%             f(:,t+1) = f(:,t) + s(:,t)*h + sigF*f(:,t).*dWf(:,t);
%             %Balloon model
%             v(:,t+1) = v(:,t) + 1/tau_0*( f(:,t) - v(:,t).^alpha )*h + sigV*v(:,t).*dWv(:,t);
%             exponent = 1./abs(f(:,t));
%             E_f = ( 1 - (1-E_0).^exponent ) / E_0;
%             q(:,t+1) = q(:,t) + 1/tau_0*( f(:,t).*E_f - (v(:,t).^(alpha-1)).*q(:,t) )*h + sigQ*q(:,t).*dWq(:,t);
%         otherwise
%             f(:,t+1) = f(:,t) + s(:,t)*h + sigF*dWf(:,t);
%             %Balloon model
%             v(:,t+1) = v(:,t) + 1/tau_0*( f(:,t) - v(:,t).^alpha )*h + sigV*dWv(:,t);
%             exponent = 1./abs(f(:,t));
%             E_f = ( 1 - (1-E_0).^exponent ) / E_0;
%             q(:,t+1) = q(:,t) + 1/tau_0*( f(:,t).*E_f - (v(:,t).^(alpha-1)).*q(:,t) )*h + sigQ*dWq(:,t);
%     end
%     if f(:,t+1)<0
%         error('ABORT! Unphysical process!!')
%     end
%     %BOLD signal
%     y(:,t+1) = V_0*(k1*(1-q(:,t+1)) + k2*(1-q(:,t+1)./v(:,t+1)) + k3*(1-v(:,t+1))) + sigY*randn;
% end
%%
for t=1:T-1
    %Neural activity
    X(:,t+1) = X(:,t) + Drift(t,X(:,t)',param,sys)'*h + SIGMA.*diffusion(X(:,t)',sys)'.*dW(:,t) ;
    if X(3,t+1)<0 || X(4,t+1)<0 
        error('ABORT! Unphysical process!!')
    end
    %BOLD signal
    y(:,t+1) = V_0*(k1*(1-X(5,t+1)) + k2*(1-X(5,t+1)./X(4,t+1)) + k3*(1-X(4,t+1))) + sigY*randn;
end

obs=1:45:T;
Yx=y(obs) ;
dim_obs=size(Yx,1);

switch plots
    case 1
% Hidden Process and Data
        z = X(1,:);
        s = X(2,:);
        f = X(3,:);
        v = X(4,:);
        q = X(5,:);
        figure
        subplot(2,3,1)
        plot(taxis,z)
        ylabel('N. Activity z')
        subplot(2,3,2)
        plot(taxis,s)
        ylabel('Vasodilatory Signal s')
        subplot(2,3,3)
        plot(taxis,f)
        ylabel('Blood Inflow f')
        subplot(2,3,4)
        plot(taxis,v)
        ylabel('Normalized Blood Volume v')
        subplot(2,3,5)
        plot(taxis,q)
        ylabel('Deoxygenated Blood (dHb) q')
        subplot(2,3,6)
        plot(taxis,y)
        hold on
        plot(obs*h,Yx,'dr','MarkerFace','r')
        hold off
        ylabel('BOLD Signal y')
    case 0
        disp('Plots disabled')
end